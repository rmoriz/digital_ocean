## Unreleased Changes

[Full Changelog](https://github.com/rmoriz/digital_ocean/compare/v0.1.0...master)

**Enhancements:**


**Bug Fixes:**


## 0.1.0 (February 20, 2013)

[Full Changelog](https://github.com/rmoriz/digital_ocean/compare/v0.0.1...v0.1.0)


**Enhancements:**

* support password reset (thanks )

* documentation

* basic YARD setup

* add changelog :)


## 0.0.1 (January 24, 2013)

[Full Changelog](https://github.com/rmoriz/digital_ocean/compare/3afd6a3c00cf447bc890703d9221fbed9662856e...v0.0.1)


**Enhancements:**

* First release


**Bug Fixes:**

* n/a

